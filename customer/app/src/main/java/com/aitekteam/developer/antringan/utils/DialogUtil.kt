package com.aitekteam.developer.antringan.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import com.aitekteam.developer.antringan.R
import com.aitekteam.developer.antringan.handlers.OnChooseImageOrPhoto

object DialogUtil {
    fun dialogChooseImageOrPhotos(activity: Activity, title: String, handler: OnChooseImageOrPhoto):
            AlertDialog.Builder {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
            .setItems(
                R.array.array_choose_image_or_photos,
                DialogInterface.OnClickListener { dialog, which ->
                    handler.onItemClick(which)
                    dialog.dismiss()
                })
        return builder
    }
}