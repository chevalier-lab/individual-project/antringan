package com.aitekteam.developer.antringan.handlers

interface OnChooseImageOrPhoto {
    fun onItemClick(position: Int)
}