package com.aitekteam.developer.antringan.utils

const val RC_SIGN_IN = 9001
const val GALLERY_REQUEST_CODE = 9002
const val CAMERA_REQUEST_CODE = 9003
const val DB_USERS = "users"